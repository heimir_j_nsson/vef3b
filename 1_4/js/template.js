$("document").ready(function(){         
            
            var template = $("#itemTemplate").html();
            // Handlebars compiles the template into a callable function
            var renderer = Handlebars.compile(template);
                        
            $.getJSON("js/employeeList.json", function(data){
            	$("#placeholder").html(renderer(data));
            });
                       
                       
});