

// skilgreini html elementin (köttur og klikker)
var elemkitten = document.getElementById('kottur');
var elemSmella = document.getElementById('smell');

// útfæri on click aðferð með closure

elemkitten.onclick = (function() {
		// upphafsstilli teljaran á 0
		var teljari = 0;
		// innra function uppfærir teljara frá ytri functioninu vegna closure 
		return function(){

			teljari++; // tel
			elemSmella.innerHTML = teljari;// sýni teljara á síðunni
		};
	})();