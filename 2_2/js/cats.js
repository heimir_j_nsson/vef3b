
// Static json data, ef $.ajax kallið er ekki að virka útaf einhverri ástæðu
 /*
 var myData = {
           "Bangsi": {
              "title": "Bangsi",
              "url": "img/Friendly.jpg"             
           }, 

           "Púki":{
              "title": "Púki",
              "url": "img/Smiley.jpg"
           }, 

           "Skjöldur":{
              "title": "Skjöldur",
              "url": "img/Mommy.jpg"
           }, 

           "Branco":{
              "title": "Branco",
              "url": "img/Scary.jpg"              
           }
        };
*/

        var click = {};  // Búum til global breytu til að geyma upplýsingar um hversu oft er búið að skoða 

    $("document").ready(function(){ 
       // - MVO skipting -  hef data Module í sér JSON skrá 
      // geymi JSON skrá á sér server sem er náð í með $.ajax
      // async verður að vera false því annars lendum við í race conditions vandamálum     
      $.ajax({
            type: "GET",
            url: "http://rei.laxdal.org/heimir_data/cats.json",
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            data: { },
            async: false,
            dataType: "json", 
            cache: true,
            success: function(data){
              //alert(data);
              window.myData = data;
            } 
        });


        // Athugum hvort clicker cookie sé til staðar
        if (typeof Cookies.get('clicker') != 'undefined'){
          // Cookie er til svo við setjum það sem er í henni inní click global breytuna okkar
          window.click = Cookies.getJSON('clicker');

        } else {
          // Cookie er ekki til svo við þurfum að búa hana til
          $.each( window.myData, function(index, value) {
            // Förum í gegnum alla kettina okkar í myData og búum til array til að geyma upplýsingar um smellina
            window.click[index] = 0;
          })
          // Búum til cookie til að geyma click breytuna okkar
          Cookies.set('clicker', window.click);
        }

        

        var listTemplate = $("#itemTemplateList").html();  // Tökum HTML gögnin úr "itemTemplateList" div-inu
        var listRenderer = Handlebars.compile(listTemplate);  // Tekur inn {{}} template  og setur í Compile fallið í Handlebars
                                                              // Compile vinnur úr gögnunm og skilar til baka FUNCTIONI inní listRenderer breytuna, 
                                                              // listRenderer er í rauninni núna orðin að falli

        var listHTMLResult = listRenderer(window.myData); // Köllum í listRenderer sem er núna orðið að falli og setjum JSON gögnin sem parameter með
                                                          // núna er listHTMLResult bara strengur með HTMLinu sem við setjum inní placeholderinn

        $("#placeholderList").html(listHTMLResult);  // Setjum HTML kóðann sem varð til úr listRenderer fallinu inní Divið

        // onclick event handler
        $("#placeholderList").on('click','li',function(event){
              var key = this.innerHTML;  // Hérna færðu key-ið
              var myCat = {};
              myCat = window.myData[key];  // Tekur upplýsingar um köttinn og setur í sér breytu

              var detailTemplate = $("#itemTemplateImage").html();  // Tekur inn handlebars templateið
              var detailRenderer = Handlebars.compile(detailTemplate);  // Compilum templateið

              var detailHTMLResult = detailRenderer(myCat);  //feedum upplýsingum um köttinn okkar í handlebars compile 
              $("#placeholderImage").html(detailHTMLResult);  // Setjum HTML kóðann okkar inní div-ið

              // Clicker logic
              // Setjum current counter inní click spanið í detail boxinu (kemur í veg fyrir að spanið sé tómt ef það er ekkert click talið)
              $( "#clickerSpan").html(window.click[key]);
              // Búum til click event á myndina
              $("#kattamynd").click(function(event){
                // hækkum click countið okkar um 1
                window.click[key]++;
                // Setjum nýju upplýsingarnar í cookie
                Cookies.set('clicker', window.click);
                // Uppfærum click countið
                $( "#clickerSpan").html(window.click[key]);
              });

          }); 
          

      });