// MODEL
'use strict';
var m = require('mithril/mithril');

var stock = module.exports = {};

stock.himage = m.request({
        method: 'GET',
        url: 'Livestock.json'
    })
    .then(JSON.parse)
    .then(function (data) {
        return data["livestock"];
    });

module.exports = stock;

// COMPONENT

'use strict';
var m = require('mithril');
var stock = require('Livestock');

var Slider = module.exports = {
    controller: function () {
        var ctrl = this;
    },

    view: function (ctrl) {
        return m(stock.himage())
};